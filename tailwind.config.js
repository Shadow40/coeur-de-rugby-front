module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    fontFamily: {
      'titre': 'Gotham',
      'text': 'Poppins'
    },
    extend: {
      colors: {
        bordeaux: "#691C33",
        business: {
          blue: "#1D2545",
          yellow: "#D79A2B",
        },
        solidarite: {
          green: "#86CAC2",
          pink: "#F3A1C6"
        },
        education: {
          blue: "#83CEEA",
          darkblue: "#2268B1"
        },
        amateur: {
          orange: "#FAC084",
          pink: "#E72A74"
        },
        eco: {
          yellow: "#FED130",
          green: "#A1D2B8"
        }
      }
    },
  },
  variants: {},
  plugins: [],
}
